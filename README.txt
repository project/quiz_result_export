About
------------

This module is an extension to the Quiz module.
Currently the quiz module does not offer export facility for the quiz admin where they can generate a report in a file.
This module exports the result of the Quiz in a csv file format. Currently we exports the following fields (UID, USERNAME, SCORE, EVALUATED, STARTED, FINISHED and DURATION)


Author(s):
  Shaggy <https://www.drupal.org/u/shaggy>

Installation
------------

1. Install and enable the module.
2. Visit the Quiz report page (admin/quiz/reports/results).
3. In the Quiz report page click on the Quiz name form list.
4. In the (node/%quiz_node/results) page click on "Download All Results" link.


How it works
------------

This module extracts the data from the quiz results table and exports the following fields (UID, USERNAME, SCORE, EVALUATED, STARTED, FINISHED and DURATION) into the file.
